from ultralytics import YOLO

def train_yolov8m_detect(n_epochs: int):
    model = YOLO('yolov8m.yaml')
    results = model.train(data='config.yaml', epochs=n_epochs)
    return results

def train_yolov8m_seg(n_epochs: int):
    model = YOLO('yolov8m-seg.yaml')
    results = model.train(data='config.yaml', epochs=n_epochs)
    return results

def train_yolov8m_obb(n_epochs: int):
    model = YOLO('yolov8m-obb.yaml')
    results = model.train(data='config.yaml', epochs=n_epochs)
    return results

def main(task: str, n_epochs: int):
    if task == 'detect':
        train_yolov8m_detect(n_epochs)
    elif task == 'seg':
        train_yolov8m_seg(n_epochs) 
    elif task == 'obb':
        train_yolov8m_obb(n_epochs)
    else:
        print("Invalid task. Please choose 'detect', 'seg' or 'obb'.")

if __name__ == '__main__':
    task = input("Enter the task (detect, seg or obb): ")
    n_epochs = int(input("Enter the number of epochs: "))
    main(task, n_epochs)
