import cv2
import os
from ultralytics import YOLO

def determine_max_resolution():
    HIGH_VALUE = 10000
    WIDTH = HIGH_VALUE
    HEIGHT = HIGH_VALUE

    capture = cv2.VideoCapture(0)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, WIDTH)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)
    width = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    
    return width, height

# Function for saving the image with a consecutive number
def save_image(image, directory):
    # Make sure that the directory exists
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Count the number of existing images in the directory
    image_count = len([name for name in os.listdir(directory) if os.path.isfile(os.path.join(directory, name))])

    # Construct the file name for the new image with the consecutive number
    filename = os.path.join(directory, f"captured_image_{image_count}.jpg")

    # Save the image under the new file name
    cv2.imwrite(filename, image)
    print(f"Bild gespeichert als {filename}")

def main():
    width, height = determine_max_resolution()
    print(width, height)

    cap = cv2.VideoCapture(0)

    # Set the resolution to the maximum supported by the webcam
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    # model = YOLO('yolov8m.pt')

    while True:
        # Reading a frame from the webcam
        ret, frame = cap.read()

        # Displaying the current frame
        cv2.imshow("Webcam", frame)

        # Waiting for a button to be pressed
        key = cv2.waitKey(1)

        # When the 's' button is pressed, you save the image
        if key == ord("s"):
            save_image(frame, "captured_images")

        # If the 'q' button is pressed, you cancel the loop
        elif key == ord("q"):
            break

    # Release the resources
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
    