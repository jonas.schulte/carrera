import cv2

# 0 Logitech webcam right USB port
# 1 Internal webcam
cap = cv2.VideoCapture(0)

# Check if the webcam is opened correctly
if not cap.isOpened():
    raise IOError("Cannot open webcam")

while True:
    ret, frame = cap.read()
    cv2.imshow('Input', frame)

    c = cv2.waitKey(1)
    if c == 27: # 27 = esc key
        break

cap.release()
cv2.destroyAllWindows()