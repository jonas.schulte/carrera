import cv2
import os
from ultralytics import YOLO

model_path = os.path.join('.', 'runs', 'detect', 'train4', 'weights', 'best.pt')

def main():
    cap = cv2.VideoCapture(0)

# ultralytics.checks()
    model = YOLO(model_path)
# model = YOLO('runs/detect/drawing_paper/weights/best.pt')
    x_line = 600
    while cap.isOpened():
    # Read a frame from the videoq
        success, frame = cap.read()
        width = int(cap.get(3))
        height = int(cap.get(4))

        if success:
        # Run YOLOv8 inference on the frameq
            resized_frame = cv2.resize(frame, (1024, 700), interpolation=cv2.INTER_LINEAR)
        # cv2.line(resized_frame, (0, x_line), (width, x_line), (255, 0, 0), 10)

        # Visualize the results on the frame
        # results = model(resized_frame, conf=0.3, classes=2)
            results = model(resized_frame)

            annotated_frame = results[0].plot()

        # Display the annotated frame
            cv2.imshow("YOLOv8 Inference", annotated_frame)

        # Break the loop if 'q' is pressed
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        else:
        # Break the loop if the end of the video is reached
            break
    return cap

cap = main()

cap.release()
cv2.destroyAllWindows()


# if __name__ == "__main__":
#     main()